import request from '@/utils/request'
import { url } from '@/utils/config'

export function fetchList(query) {
  return request({
    url: url + 'api/alluser',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/article/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: '/article/create',
    method: 'post',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: url + 'api/profile/update/' + data.get('id'),
    method: 'post',
    data
  })
}



