import request from '@/utils/request'
import { url } from '@/utils/config'

export function getAllArtist(query) {
  return request({
    url: url + 'api/artist',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/article/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function addArtist(data) {
  return request({
    url: url + 'api/add/artist',
    method: 'post',
    data
  })
}

export function deleteArtist(id) {
  return request({
    url: url + 'api/artist/delete/' + id,
    method: 'delete'
  })
}

export function updateArtist(data) {
  return request({
    url: url + 'api/edit/artist/' + data.get('id'),
    method: 'post',
    data
  })
}

export function searchArtist(name) {
  return request({
    url: url + 'api/artist/search?title=' + name,
    method: 'get'
  })
}

export function blockArtist(id) {
  return request({
    url: url + 'api/artist/block/' + id,
    method: 'post'
  })
}

export function unblockArtist(id) {
  return request({
    url: url + 'api/artist/unblock/' + id,
    method: 'post'
  })
}
