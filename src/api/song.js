import request from '@/utils/request'
import { url } from '@/utils/config'

export function getAllSong(query) {
  return request({
    url: url + 'api/allsong',
    method: 'get',
    params: query
  })
}

export function createSong(data) {
  return request({
    url: url + 'api/add/song',
    method: 'post',
    data
  })
}

export function updateSong(data) {
  return request({
    url: url + 'api/edit/song/' + data.get('id'),
    method: 'post',
    data
  })
}

export function deleteSong(id) {
  return request({
    url: url + 'api/song/delete/' + id,
    method: 'delete'
  })
}

export function searchSong(title) {
  return request({
    url: url + 'api/song/search?title=' + title,
    method: 'get'
  })
}

export function getPopularSong() {
  return request({
    url: url + 'api/log/count',
    method: 'get'
  })
}

export function getAllRecentlyPlayed() {
  return request({
    url: url + 'api/log',
    method: 'get'
  })
}

export function blockSong(id) {
  return request({
    url: url + 'api/song/block/' + id,
    method: 'post'
  })
}

export function unblockSong(id) {
  return request({
    url: url + 'api/song/unblock/' + id,
    method: 'post'
  })
}

export function getRecomendationSong(data) {
  return request({
    url: url + 'api/recomendation',
    method: 'get',
    params: data
  })
}

export function calculateRecommendationUser(data) {
  return request({
    url: url + 'api/calculate/recomendation',
    method: 'get',
    params: data
  })
}

export function calculateRecommendationAllUser(data) {
  return request({
    url: url + 'api/calculate_all_user/recomendation',
    method: 'get',
    params: data
  })
}