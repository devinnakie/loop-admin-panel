import request from '@/utils/request'
import { url } from '@/utils/config'

export function getAllUser(query) {
  return request({
    url: url + 'api/alluser',
    method: 'get',
    params: query
  })
}

export function login(data) {
  return request({
    url: url + 'api/login',
    // url: '/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'get'
  })
}

export function searchUser(name) {
  return request({
    url: url + 'api/user/search?name=' + name,
    method: 'get'
  })
}

export function addUser(data) {
  return request({
    url: url + 'api/register',
    method: 'post',
    data
  })
}

export function blockUser(id) {
  return request({
    url: url + 'api/user/block/' + id,
    method: 'post'
  })
}

export function unblockUser(id) {
  return request({
    url: url + 'api/user/unblock/' + id,
    method: 'post'
  })
}
