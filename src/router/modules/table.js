/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const tableRouter = {
  path: '/table',
  component: Layout,
  redirect: '/table/user',
  name: 'Table',
  meta: {
    title: 'Table',
    icon: 'table'
  },
  children: [
    // {
    //   path: 'dynamic-table',
    //   component: () => import('@/views/table/dynamic-table/index'),
    //   name: 'DynamicTable',
    //   meta: { title: 'Dynamic Table' }
    // },
    // {
    //   path: 'drag-table',
    //   component: () => import('@/views/table/drag-table'),
    //   name: 'DragTable',
    //   meta: { title: 'Drag Table' }
    // },
    // {
    //   path: 'inline-edit-table',
    //   component: () => import('@/views/table/inline-edit-table'),
    //   name: 'InlineEditTable',
    //   meta: { title: 'Inline Edit' }
    // },
    {
      path: 'user',
      component: () => import('@/views/table/User'),
      name: 'User',
      meta: { title: 'User' }
    },
    {
      path: 'song',
      component: () => import('@/views/table/Song'),
      name: 'Song',
      meta: { 
        title: 'Song',
        icon: 'table'

      }
    },
    {
      path: 'artist',
      component: () => import('@/views/table/Artist'),
      name: 'Artist',
      meta: { title: 'Artist' }
    },
    {
      path: 'z',
      component: () => import('@/views/table/Testing'),
      name: 'Testing',
      meta: { title: 'Testing' }
    }
  ]
}
export default tableRouter
